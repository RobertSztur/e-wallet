<%@ page import="com.craftincode.Services.AccountService" %>
<%@ page import="com.craftincode.Model.Account" %><%--
  Created by IntelliJ IDEA.
  User: rober
  Date: 2019-10-01
  Time: 13:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Create-Transaction</title>
</head>
<body>
<h1>New Transaction</h1>
<%
    String accountId = request.getParameter("accountId");
%>
<form method="post" action="createTransaction">
    <input type="hidden" value="<%=accountId%>" name="accountId">

    <label>
        Description:
        <input name="description" type="text">
    </label>
    <br>
    <label>
        Amount:
        <input type="number" name="amount" value="0">
    </label>
    <br>
    <input type="submit" value="Create"/>
</form>
<a href="allusers.jsp">Go to main menu</a>
</body>
</html>
