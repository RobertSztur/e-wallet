package DB;

import java.sql.Connection;
import java.sql.DriverManager;

public class DatabaseConnect {
    public static final String JDBC_MAGIC = "?autoReconnect=true&useSSL=false&serverTimezone=UTC";
    public static final String JDBC_URL = "jdbc:mysql://localhost:3306/e_wallet" + JDBC_MAGIC;
    public static final String JDBC_USER = "root";
    public static final String JDBC_PASSWORD = "root";

    public static Connection createConnection(){

        try {
            // załadowanie sterownika
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            // otworzenie połączenia
            return DriverManager.getConnection(JDBC_URL, JDBC_USER, JDBC_PASSWORD);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
