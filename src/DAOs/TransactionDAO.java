package DAOs;

import DB.DatabaseConnect;
import com.craftincode.Model.Transaction;
import com.craftincode.Services.IdGenerator;

import javax.ejb.Stateless;
import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class TransactionDAO {

    public  List<Transaction> getAllTransactions() {
        List<Transaction> accounts = new ArrayList<>();
        Connection connection = DatabaseConnect.createConnection();

        try {
            PreparedStatement ps = connection.prepareStatement("select * from transaction");
            ResultSet rs = ps.executeQuery();

            while (rs.next()){
                Transaction account = createTransactionFromResultSet(rs);
                accounts.add(account);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        return accounts;
    }

    private  Transaction createTransactionFromResultSet(ResultSet rs) throws SQLException {
        String id = rs.getString("id");
        String description = rs.getString("description");
        BigDecimal amount = rs.getBigDecimal("amount");
        Date date = rs.getDate("date");
        String categoryId = rs.getString("transaction_category_id");
        String accountId = rs.getString("account_id");
        String userId = rs.getString("user_id");

        return new Transaction(id,description ,amount,userId, accountId, categoryId, date);
    }


    public  void add(Transaction transaction) {
        Connection connection = DatabaseConnect.createConnection();

        try {
            PreparedStatement ps = connection.prepareStatement("insert into transaction values (?,?,?,?,?,?,?);");

            ps.setString(1, transaction.getId());
            ps.setString(2, transaction.getDescription());
            ps.setBigDecimal(3, transaction.getAmount());
            ps.setDate(4, new java.sql.Date(transaction.getDate().getTime()));
            ps.setString(5, transaction.getCategoryId());
            ps.setString(6, transaction.getAccountId());
            ps.setString(7, transaction.getUserId());

            ps.executeUpdate();

            System.out.println("Transaction " + ps + " inserted to DB!");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public  void delete(String id) {
        Connection connection = DatabaseConnect.createConnection();

        try {
            PreparedStatement ps = connection.prepareStatement("delete from transaction where id = ?;");
            ps.setString(1, id);

            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public  List<Transaction> getTransactionsForAccount(String accountId) {
        List<Transaction> transactions = new ArrayList<>();
        Connection connection = DatabaseConnect.createConnection();

        try {
            PreparedStatement ps = connection.prepareStatement("select * from transaction where account_id =?;");
            ps.setString(1, accountId);
            ResultSet rs = ps.executeQuery();

            while (rs.next()){
                Transaction transaction = createTransactionFromResultSet(rs);
                transactions.add(transaction);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return transactions;
    }

    public  List<Transaction> getAllUserTransactions(String userId) {
        List<Transaction> allUserTransactions = new ArrayList<>();
        Connection connection = DatabaseConnect.createConnection();

        try {
            PreparedStatement ps = connection.prepareStatement("select * from transaction where user_id =?;");
            ps.setString(1, userId);
            ResultSet rs = ps.executeQuery();

            while (rs.next()){
                Transaction transaction = createTransactionFromResultSet(rs);
                allUserTransactions.add(transaction);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return allUserTransactions;
    }

    public  Transaction getById(String transactionId) {
        List<Transaction> transactionById = new ArrayList<>();
        Connection connection = DatabaseConnect.createConnection();

        try {
            PreparedStatement ps = connection.prepareStatement("select * from transaction where id =?;");
            ps.setString(1, transactionId);
            ResultSet rs = ps.executeQuery();

            if (rs.next()){
                return createTransactionFromResultSet(rs);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
