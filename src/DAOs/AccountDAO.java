package DAOs;

import DB.DatabaseConnect;
import com.craftincode.Model.Account;

import javax.ejb.Stateless;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
@Stateless
public class AccountDAO {

    public static final String QUERY_GET_ALL_ACCOUNTS = "select * from account;";
    public static final String QUERY_GET_ALL_USERS_ACCOUNTS = "select * from account \n" +
            "    join user_account ua on account.id = ua.account_id\n" +
            "    where user_id = ?;";


    private Account createAccountFromResultSet(ResultSet rs) throws SQLException {
        String id = rs.getString("id");
        String name = rs.getString("name");
        BigDecimal balance = rs.getBigDecimal("balance");

        return new Account(id, name, balance,null);
    }



    public List<Account> getAllAccounts() {
        List<Account> accounts = new ArrayList<>();
        Connection connection = DatabaseConnect.createConnection();
        try {
            PreparedStatement ps = connection.prepareStatement(QUERY_GET_ALL_ACCOUNTS);
            ResultSet rs = ps.executeQuery();
            while (rs.next()){
                Account account = createAccountFromResultSet(rs);
                accounts.add(account);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return accounts;
    }

    public void addAccount(Account account) {
        Connection connection = DatabaseConnect.createConnection();

        try {
            PreparedStatement ps = connection.prepareStatement("insert into account values (?, ?, ?);");
            ps.setString(1, account.getId());
            ps.setString(2, account.getName());
            ps.setString(3, String.valueOf(account.getBalance()));

            ps.executeUpdate();

            System.out.println("Account " + ps + " inserted to DB!");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Account> getAllUserAccounts(String id) {
        List<Account> accounts = new ArrayList<>();
        Connection connection = DatabaseConnect.createConnection();

        try {
            assert connection != null;
            PreparedStatement ps = connection.prepareStatement(QUERY_GET_ALL_USERS_ACCOUNTS);
            ps.setString(1, id);
            ResultSet rs = ps.executeQuery();

            while (rs.next()){
                Account account = createAccountFromResultSet(rs);
                accounts.add(account);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        return accounts;
    }

    public Account getAccountById(String id) {
        Connection connection = DatabaseConnect.createConnection();
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement("select * from account where id = ?;");
            ps.setString(1,id);
            ResultSet resultSet = ps.executeQuery();

            if (resultSet.next()) {
                return createAccountFromResultSet(resultSet);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    public void updateBalance(String accountId, BigDecimal balance) {
        Connection connection = DatabaseConnect.createConnection();

        try {
            PreparedStatement ps = connection.prepareStatement("update account set balance = ? where id = ?;");
            ps.setBigDecimal(1, balance);
            ps.setString(2, accountId);

            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
