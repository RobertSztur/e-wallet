package DAOs;

import DB.DatabaseConnect;
import com.craftincode.Model.TransactionCategory;
import com.craftincode.Services.IdGenerator;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TransactionCategoryDAO {
    public static List<TransactionCategory> getAll() {
        List<TransactionCategory> transactionCategories = new ArrayList<>();
        Connection connection = DatabaseConnect.createConnection();

        try {
            PreparedStatement ps = connection.prepareStatement("select * from transaction_category");
            ResultSet rs = ps.executeQuery();

            while (rs.next()){
               TransactionCategory transactionCategory = createTransactionFromResultSet(rs);
                transactionCategories.add(transactionCategory);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return transactionCategories;
    }

    private static TransactionCategory createTransactionFromResultSet(ResultSet rs) throws SQLException {
        String id = rs.getString("id");
        String name = rs.getString("name");
        return new TransactionCategory(id,name);
    }

    public static TransactionCategory getCategoryOfTransaction(String transactionId) {
        Connection connection = DatabaseConnect.createConnection();
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement("select * from transaction_category where id = ?;");
            ps.setString(1,transactionId);
            ResultSet resultSet = ps.executeQuery();

            if (resultSet.next()) {
                return createTransactionFromResultSet(resultSet);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void createNew(TransactionCategory transactionCategory) {
        Connection connection = DatabaseConnect.createConnection();
        try {
            PreparedStatement ps = connection.prepareStatement("insert into transaction_category values (?, ?);");
            String randomId = IdGenerator.generateId();
            ps.setString(1,randomId);
            ps.setString(2,transactionCategory.getName());
            ps.executeUpdate();

            System.out.println("Transaction category " + ps + " inserted to DB!");
        } catch (SQLException e) {
            e.printStackTrace();
        }
}


    public static void delete(String id) {
        Connection connection = DatabaseConnect.createConnection();

        try {
            PreparedStatement ps = connection.prepareStatement("delete from transaction_category where id = ?;");
            ps.setString(1, id);

            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static TransactionCategory getById(String categoryId) {
        List<TransactionCategory> transactionById = new ArrayList<>();
        Connection connection = DatabaseConnect.createConnection();

        try {
            PreparedStatement ps = connection.prepareStatement("select * from transaction where id =?;");
            ps.setString(1, categoryId);
            ResultSet rs = ps.executeQuery();

            if (rs.next()){
                return createTransactionFromResultSet(rs);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
}

