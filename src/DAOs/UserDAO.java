package DAOs;

import DB.DatabaseConnect;
import com.craftincode.Model.User;

import javax.ejb.Stateless;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
@Stateless
public class UserDAO {
    public List<User> getAllUsers() {
        List<User> users = new ArrayList<>();
// nawiązanie połączenia do bazy danych
        Connection connection = DatabaseConnect.createConnection();
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement("select * from user;");
            ResultSet resultSet = ps.executeQuery(); // TYLKO dla SELECTów

            // pętla przechodząca po wszystkich wierszach wyniku zapytania
            while (resultSet.next()) {
                User userFromDb = userFromResultSet(resultSet);
                // dodanie użytkownika odczytanego z bazy do listy wszystkich użytkowników
                users.add(userFromDb);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return users;
    }
    private User userFromResultSet(ResultSet resultSet) throws SQLException {
        // wyciągnięcie danych z komórek aktualnego wiersza
        String id = resultSet.getString("id");
        String name = resultSet.getString("name");
        String surname = resultSet.getString("surname");
        String login = resultSet.getString("login");
        String password = resultSet.getString("password");
        String role = resultSet.getString("role");
        String email = resultSet.getString("email");

        // zamienić dane z bazy na obiekt klasy User
        return new User(id, name, surname, login, password, role, email);
    }

    public void addUser(User user) {
        Connection connection = DatabaseConnect.createConnection();
        try {
            PreparedStatement ps = connection.prepareStatement("insert into user values (?, ?, ?, ?, ?, ?, ?);");
            ps.setString(1, user.getId()); // za pierwszy znak zapytania podstaw user.getId()...
            ps.setString(2, user.getName());
            ps.setString(3, user.getSurname());
            ps.setString(4, user.getLogin());
            ps.setString(5, user.getPassword());
            ps.setString(6, user.getRole());
            ps.setString(7, user.getEmail());

            ps.executeUpdate(); // dla insert, update, delete

            System.out.println("User " + user + " inserted to DB!");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public User findUserById(String id) {
        Connection connection = DatabaseConnect.createConnection();

        PreparedStatement ps = null;
        try {
            assert connection != null;
            ps = connection.prepareStatement("select * from user where id = ?;");
            ps.setString(1, id);

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                return userFromResultSet(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    public User findUserByLogin(String login) {
        Connection connection = DatabaseConnect.createConnection();

        PreparedStatement ps = null;
        try {
            assert connection != null;
            ps = connection.prepareStatement("select * from user where login = ?;");

            ps.setString(1, login);

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                return userFromResultSet(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void deleteUserById(String id) {
        Connection connection = DatabaseConnect.createConnection();

        try {
            PreparedStatement ps = connection.prepareStatement("delete from user where id = ?;");
            ps.setString(1, id);

            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateUser(String id, String name, String surname, String password, String role, String email) {
        Connection connection = DatabaseConnect.createConnection();

        PreparedStatement ps = null;
        try {
            assert connection != null;
            ps = connection.prepareStatement("update user set name=?,surname=?,password=?,role=?, email=? WHERE id=? ;");

            ps.setString(1, name);
            ps.setString(2, surname);
            ps.setString(3, password);
            ps.setString(4, role);
            ps.setString(5, email);
            ps.setString(6, id);

            ps.executeUpdate();
            } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void setUserRole(String userId, String role) {
        Connection connection = DatabaseConnect.createConnection();

        try {
            PreparedStatement ps = connection.prepareStatement("update user set role = ? where id = ?;");
            ps.setString(1, role);
            ps.setString(2, userId);

            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
