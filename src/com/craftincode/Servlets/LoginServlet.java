package com.craftincode.Servlets;

import DAOs.UserDAO;
import com.craftincode.Model.User;
import com.craftincode.Services.UserService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/loginServlet")
public class LoginServlet extends HttpServlet {
    @EJB
    UserDAO userDAO;
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setContentType("text/html;charset=UTF-8");

        String login = req.getParameter("login");
        String password = req.getParameter("password");

        User userByLogin = userDAO.findUserByLogin(login);
        if (userByLogin != null && userByLogin.getPassword().equals(password)) {
        req.getSession().setAttribute("username", login);
        resp.sendRedirect(resp.encodeRedirectURL("homepage"));
    } else {
        resp.sendRedirect(resp.encodeRedirectURL("login.html"));
    }
}
}
