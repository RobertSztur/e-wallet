package com.craftincode.Servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet ("/homepage")
public class HomepageServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=utf-8");

        resp.getWriter().write("Welcome to HOMEPAGE!");
        resp.getWriter().write("<hr>");
        resp.getWriter().write("<a href=\"all-accounts\">All accounts!</a>");
        resp.getWriter().write("<br><a href=\"user-accounts\">Your accounts!</a>");
        resp.getWriter().write("<br> <a href=\"delete-account\">Delete account!</a>");
        resp.getWriter().write("<hr>");
        resp.getWriter().write("<a href=\"allusers\">Users list</a>");
        resp.getWriter().write("<br><a href=\"users-table\">Users-table</a>");
        resp.getWriter().write("<br><a href=\"users-api\">Users table (MVC)</a>");
        resp.getWriter().write("<hr>");
        resp.getWriter().write("<hr><a href=\"testDB\">Database check!</a>");
        resp.getWriter().write("<br><a href=\"register.html\">Registration form</a>");
        resp.getWriter().write("<br><a href=\"logout\">Logout</a>");
    }
}
