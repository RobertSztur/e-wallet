package com.craftincode.Servlets;

import DAOs.UserDAO;
import com.craftincode.Services.UserService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/editData")
public class EditDataServlet extends HttpServlet {
    @EJB
    UserDAO userDAO;
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setContentType("text/html;charset=UTF-8");

        String id = req.getParameter("id");
        String name = req.getParameter("name");
        String surname = req.getParameter("surname");
        String password = req.getParameter("password");
        String role = req.getParameter("role");
        String email = req.getParameter("email");

        userDAO.updateUser(id, name, surname, password, role, email);
        resp.sendRedirect(resp.encodeRedirectURL("allusers.jsp"));
        resp.getWriter().write("<br><a href=\"allusers.jsp\">Go to main menu</a>");
    }
}
