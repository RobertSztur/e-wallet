package com.craftincode.Servlets;

import DAOs.AccountDAO;
import com.craftincode.Model.Account;
import com.craftincode.Services.AccountService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/all-accounts")
public class AllAccountsServlet extends HttpServlet {
    @EJB
    private AccountDAO accountDAO;
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Account> accounts = accountDAO.getAllAccounts();

        req.setAttribute("accounts",accounts);
        req.getRequestDispatcher("all-accounts.jsp").forward(req,resp);
    }
}
