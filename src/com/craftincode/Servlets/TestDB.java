package com.craftincode.Servlets;

import DB.DatabaseConnect;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;

@WebServlet("/testDB")
public class TestDB extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Connection connection = DatabaseConnect.createConnection();
        if (connection == null){
            resp.getWriter().write("DB failed :(");
        } else{
            resp.getWriter().write("DB connect works :)");
        }
    }
}
