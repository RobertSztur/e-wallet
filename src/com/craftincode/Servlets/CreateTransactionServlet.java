package com.craftincode.Servlets;

import DAOs.AccountDAO;
import DAOs.TransactionDAO;
import DAOs.UserDAO;
import com.craftincode.Model.Transaction;
import com.craftincode.Model.User;
import com.craftincode.Services.AccountService;
import com.craftincode.Services.TransactionService;
import com.craftincode.Services.UserService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;


@WebServlet("/createTransaction")
public class CreateTransactionServlet extends HttpServlet {
    @EJB
    UserDAO userDAO;
    @EJB
    private AccountDAO accountDAO;
    @EJB
    private TransactionDAO transactionDAO;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String accountId = req.getParameter("accountId");
        String description = req.getParameter("description");
        String amount = req.getParameter("amount");
        BigDecimal amountDecimaal = new BigDecimal(amount);

        String userLogin = (String) req.getSession().getAttribute("username");
        User user = userDAO.findUserByLogin(userLogin);
        String userId = user.getId();

        Transaction transaction = new Transaction(null, description, amountDecimaal, userId, accountId);
        transactionDAO.add(transaction);

        resp.sendRedirect(resp.encodeRedirectURL("account-details.jsp?id=" + accountId));



    }
}

