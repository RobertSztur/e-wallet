package com.craftincode.Servlets;

import DAOs.UserDAO;
import com.craftincode.Services.UserService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
@WebServlet("/changeRole")
public class ChangeRoleServlet extends HttpServlet {
    @EJB
    UserDAO userDAO;
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String userId = req.getParameter("id");
        String role = req.getParameter("role");

        userDAO.setUserRole(userId, role);
        resp.sendRedirect(resp.encodeRedirectURL("allusers.jsp"));

    }
}

