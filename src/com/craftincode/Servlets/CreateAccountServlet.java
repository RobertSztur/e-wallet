package com.craftincode.Servlets;

import DAOs.AccountDAO;
import DAOs.UserDAO;
import com.craftincode.Model.Account;
import com.craftincode.Model.User;
import com.craftincode.Services.AccountService;
import com.craftincode.Services.UserService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;

@WebServlet("/CreateAccountServlet")
public class CreateAccountServlet extends HttpServlet {
    @EJB
    private AccountDAO accountDAO;
    @EJB
    UserDAO userDAO;
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");

        String name = req.getParameter("name");
        String balance = req.getParameter("balance");
        BigDecimal balanceDecimal = new BigDecimal(balance);

        String userLogin = (String) req.getSession().getAttribute("username");
        User user = userDAO.findUserByLogin(userLogin);
        String userId = user.getId();

        Account account = new Account(userLogin,name,balanceDecimal,userId);
        accountDAO.addAccount(account);

        resp.sendRedirect(resp.encodeRedirectURL("all-accounts"));
    }
}
