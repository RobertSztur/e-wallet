package com.craftincode.Servlets;

import DAOs.UserDAO;
import com.craftincode.Model.User;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/delete-account")

public class DeleteAccountServlet extends HttpServlet {
    @EJB
    UserDAO userDAO;
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();

        String login = (String) session.getAttribute("username");

        User loggedUser = userDAO.findUserByLogin(login);

        userDAO.deleteUserById(loggedUser.getId());

        session.invalidate();

        resp.sendRedirect(resp.encodeRedirectURL(req.getContextPath() + "/login.html"));
    }

}
