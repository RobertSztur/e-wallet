package com.craftincode.Servlets;

import DAOs.UserDAO;
import com.craftincode.Model.User;
import com.craftincode.Services.UserService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@WebServlet("/users")
public class UserServlet extends HttpServlet{
    @EJB
    UserDAO userDAO;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=UTF-8");

        List<User> allUsers = userDAO.getAllUsers();

        for (User user : allUsers) {
            resp.getWriter().write(user + "<br>");
        }


    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=UTF-8");

        req.setCharacterEncoding("utf-8");
        resp.setContentType("text/html;charset=UTF-8");

        String id = req.getParameter("id");
        String name = req.getParameter("name");
        String surname = req.getParameter("surname");
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        String role = req.getParameter("role");
        String email = req.getParameter("email");

        User newUser = new User(id, name, surname, login, password, role, email);
        userDAO.addUser(newUser);

        resp.getWriter().write("User has been added!");
        resp.getWriter().write("<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>login page</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "<h1>Welcome to login page!</h1>\n" +
                "    <form method=\"post\" action=\"loginServlet\">\n" +
                "        Login:\n" +
                "        <input type=\"text\" name=\"login\">\n" +
                "        <br>\n" +
                "        Password:\n" +
                "        <input type=\"password\" name=\"password\">\n" +
                "        <br>\n" +
                "        <input type=\"submit\" value=\"Login\">\n" +
                "    </form>\n" +
                "</body>\n" +
                "</html>");
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setContentType("text/html;charset=UTF-8");

        String id = req.getParameter("id");
        String name = req.getParameter("name");
        String surname = req.getParameter("surname");
        String password = req.getParameter("password");
        String role = req.getParameter("role");
        String email = req.getParameter("email");

        userDAO.updateUser(id, name, surname, password, role, email);
        resp.getWriter().write("User has been updated!");

    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=UTF-8");

        String id = req.getParameter("id");
        userDAO.deleteUserById(id);

        resp.getWriter().write("User has been deleted!");

    }
}
