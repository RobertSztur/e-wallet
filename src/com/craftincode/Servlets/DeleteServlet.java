package com.craftincode.Servlets;

import DAOs.UserDAO;
import com.craftincode.Services.UserService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/delete")

public class DeleteServlet extends HttpServlet {
    @EJB
    UserDAO userDAO;
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String userId = req.getParameter("id");

        userDAO.deleteUserById(userId);

        resp.sendRedirect(resp.encodeRedirectURL("allusers.jsp"));
    }

}
