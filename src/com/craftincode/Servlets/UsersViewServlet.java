package com.craftincode.Servlets;

import DAOs.UserDAO;
import com.craftincode.Model.User;
import com.craftincode.Services.UserService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@WebServlet("/users-api")
public class UsersViewServlet extends HttpServlet {
    @EJB
    UserDAO userDAO;
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<User> allUsers = userDAO.getAllUsers();

        req.setAttribute("users",allUsers);

        req.getRequestDispatcher("allusers.jsp").

                forward(req, resp);
    }
}
