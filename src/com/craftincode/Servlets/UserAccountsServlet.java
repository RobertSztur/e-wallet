package com.craftincode.Servlets;

import DAOs.AccountDAO;
import DAOs.UserDAO;
import com.craftincode.Model.Account;
import com.craftincode.Model.User;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

@WebServlet("/user-accounts")
public class UserAccountsServlet extends HttpServlet {
    @EJB
    UserDAO userDAO;
    @EJB
    private AccountDAO accountDAO;
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = (String) req.getSession().getAttribute("username");
        User userByLogin = userDAO.findUserByLogin(username);
        List<Account> userAccount = accountDAO.getAllUserAccounts(userByLogin.getId());

        BigDecimal sum = BigDecimal.ZERO;
        for (Account account : userAccount) {
            sum = sum.add(account.getBalance());
        }

        req.setAttribute("sum", sum);
        req.setAttribute("accounts", userAccount);

        req.getRequestDispatcher("/user-accounts.jsp").forward(req, resp);
    }
}

