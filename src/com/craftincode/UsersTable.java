package com.craftincode;

import DAOs.UserDAO;
import com.craftincode.Model.User;
import com.craftincode.Services.UserService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/users-table")
public class UsersTable extends HttpServlet {
    @EJB
    UserDAO userDAO;
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=UTF-8");

        resp.getWriter().write("<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>Title</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "\n" +
                "<h1>All users</h1>\n" +
                "\n" +
                "<table>\n" +
                "    <tr>\n" +
                "        <th>Id</th>\n" +
                "        <th>Login</th>\n" +
                "        <th>Name</th>\n" +
                "        <th>Surname</th>\n" +
                "        <th>Role</th>\n" +
                "        <th>Email</th>\n" +
                "    </tr>\n");

        List<User> allUsers = userDAO.getAllUsers();
        // pętla, która dla każdego użytkownika dokleja nowy wiersz HTML (<tr></tr>) z danymi tego użytkownika
        for (User user : allUsers) {

            resp.getWriter().write("<!--    Dane kolejnych użytkowników-->\n" +
                    "    <tr>\n" +
                    "        <td>" + user.getId() + "</td>\n" +
                    "        <td>" + user.getLogin() + "</td>\n" +
                    "        <td>" + user.getName() + "</td>\n" +
                    "        <td>" + user.getSurname() + "</td>\n" +
                    "        <td>" + user.getRole() + "</td>\n" +
                    "        <td>" + user.getEmail() + "</td>\n" +
                    "    </tr>\n");
        }


        resp.getWriter().write("</table>\n" +
                "\n" +
                "</body>\n" +
                "</html>\n");
    }
}
