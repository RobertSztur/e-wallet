package com.craftincode;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;

@WebServlet("/sessionhomepage")
public class SessionHomepage extends HttpServlet {

    protected void doPost(HttpServletRequest req, HttpServletResponse response) throws IOException, ServletException{
        response.setContentType("text/html;charset=UTF-8");

        HttpSession session = req.getSession();
        String user = (String) session.getAttribute("user");
        if (user != null) {
            response.getWriter().write("Welcome to HOMEPAGE!");
        }else {
            response.getWriter().write("You have to login to view this page!");
        }
    }
    protected void doGet(HttpServletRequest req, HttpServletResponse response) throws IOException, ServletException {
        doPost(req,response);

        doPost(req,response);
    }
}
