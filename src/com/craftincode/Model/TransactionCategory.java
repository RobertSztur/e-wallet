package com.craftincode.Model;

import java.util.ArrayList;
import java.util.List;

public class TransactionCategory {
    private String id;
    private String name;
    private List<String> transactionIds = new ArrayList<>();


    public TransactionCategory(String id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return "TransactionCategory{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", transactionIds=" + transactionIds +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getTransactionIds() {
        return transactionIds;
    }

    public void setTransactionIds(List<String> transactionIds) {
        this.transactionIds = transactionIds;
    }

}
