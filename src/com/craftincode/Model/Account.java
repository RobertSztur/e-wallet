package com.craftincode.Model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Account {
    private String id;
    private String name;
    private BigDecimal balance = BigDecimal.ZERO;
    private List<String> userIds = new ArrayList<>();
    private List<String> transactionIds = new ArrayList<>();

    public List<String> getTransactionIds() {
        return transactionIds;
    }

    public void setTransactionIds(List<String> transactionIds) {
        this.transactionIds = transactionIds;
    }

    public Account(String id, String name, BigDecimal balance, String userId) {
        this.id = id;
        this.name = name;
        this.balance = balance;
        this.userIds.add(userId);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Account account = (Account) o;

        if (id != null ? !id.equals(account.id) : account.id != null) return false;
        if (name != null ? !name.equals(account.name) : account.name != null) return false;
        if (balance != null ? !balance.equals(account.balance) : account.balance != null) return false;
        if (userIds != null ? !userIds.equals(account.userIds) : account.userIds != null) return false;
        return transactionIds != null ? transactionIds.equals(account.transactionIds) : account.transactionIds == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (balance != null ? balance.hashCode() : 0);
        result = 31 * result + (userIds != null ? userIds.hashCode() : 0);
        result = 31 * result + (transactionIds != null ? transactionIds.hashCode() : 0);
        return result;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getBalance() {
        return balance;
    }


    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public List<String> getUserIds() {
        return userIds;
    }


    public void setUserIds(List<String> userIds) {
        this.userIds = userIds;
    }

    @Override
    public String toString() {
        return "Account{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", balance=" + balance +
                ", userIds=" + userIds +
                ", transactionIds=" + transactionIds +
                '}';
    }
}
