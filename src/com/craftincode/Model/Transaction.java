package com.craftincode.Model;

import java.math.BigDecimal;
import java.util.Date;

public class Transaction {
    private String id;
    private String description;
    private BigDecimal amount = BigDecimal.ZERO;
    private String userId;
    private String accountId;
    private String categoryId;
    private Date date;

    public Transaction(String id, String description, BigDecimal amount, String userId, String accountId, String categoryId, Date date) {
        this.id = id;
        this.description = description;
        this.amount = amount;
        this.userId = userId;
        this.accountId = accountId;
        this.categoryId = categoryId;
        this.date = date;
    }


    @Override
    public String toString() {
        return "Transaction{" +
                "id='" + id + '\'' +
                ", description='" + description + '\'' +
                ", amount=" + amount +
                ", userId='" + userId + '\'' +
                ", accountId='" + accountId + '\'' +
                ", categoryId='" + categoryId + '\'' +
                ", date=" + date +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Transaction(String id, String description, BigDecimal amount, String userId, String accountId) {
        this.id = id;
        this.description = description;
        this.amount = amount;
        this.userId = userId;
        this.accountId = accountId;
        this.date = new Date();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}

