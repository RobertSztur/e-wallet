package com.craftincode.Services;

import DAOs.TransactionDAO;
import com.craftincode.Model.Transaction;

import javax.ejb.EJB;
import java.util.List;

public class TransactionService {

    @EJB
    private TransactionDAO transactionDAO;

    public  List<Transaction> getAll(){
        return transactionDAO.getAllTransactions();
    }

    public  List<Transaction> getTransactionsForAccount(String accountId){
        return transactionDAO.getTransactionsForAccount(accountId);
    }

    public  void delete(String id){
        transactionDAO.delete(id);
    }

    public  void add(Transaction transaction) {
        transactionDAO.add(transaction);
    }
    public  Transaction getById(String transactionId) {
       return transactionDAO.getById(transactionId);
    }


    public  List<Transaction> getAllUserTransactions(String userId) {
       return transactionDAO.getAllUserTransactions(userId);
    }
}
