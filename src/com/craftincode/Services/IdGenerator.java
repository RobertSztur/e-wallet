package com.craftincode.Services;

import java.util.UUID;

public class IdGenerator {
    public static String generateId(){
        String uuid = UUID.randomUUID().toString();
        return uuid.substring(0,5);
    }
}
