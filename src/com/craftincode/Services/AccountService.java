package com.craftincode.Services;

import DAOs.AccountDAO;
import DAOs.TransactionDAO;
import com.craftincode.Model.Account;
import com.craftincode.Model.Transaction;

import javax.ejb.EJB;
import java.math.BigDecimal;
import java.util.List;

public class AccountService {
@EJB
private AccountDAO accountDAO;
@EJB
private TransactionDAO transactionDAO;

    public List<Account> getAllAccounts() {
        return accountDAO.getAllAccounts();
    }


        public List<Account> getAllUserAccounts (String id){
            return accountDAO.getAllUserAccounts(id);
        }


    public Account getById(String id) {
        return accountDAO.getAccountById(id);
    }

   public void addNew(Account account){
       accountDAO.addAccount(account);
    }

    public void addNewTransaction(String accountId, Transaction transaction) {
        // ustawienie losowego ID transakcji
        transaction.setId(IdGenerator.generateId());

        Account account = getById(accountId);
        if (account != null) {
            // pobranie aktualnego stanu konta i kwoty transakcji
            BigDecimal oldBalance = account.getBalance();
            BigDecimal transactionAmount = transaction.getAmount();
            // ustawienie nowego stanu konta
            account.setBalance(oldBalance.add(transactionAmount));

            // dodanie do konta ID transakcji
            account.getTransactionIds().add(transaction.getId());
            // ustawienie ID konta w transakcji
            transaction.setAccountId(accountId);

            accountDAO.updateBalance(accountId, account.getBalance());

            // dodanie transakcji do listy transakcji
            transactionDAO.add(transaction);
        }
    }
}
