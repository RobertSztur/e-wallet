package com.craftincode.Services;

import DAOs.UserDAO;
import com.craftincode.Model.User;
import javax.ejb.EJB;
import java.util.List;
import java.util.UUID;

public class UserService {
@EJB
UserDAO userDAO;
private static String generateId() {
    return UUID.randomUUID().toString().substring(0, 6);
}

    public void addNewUser(User user) {
        userDAO.addUser(user);
    }

    public  List<User> getAllUsers() {
        return userDAO.getAllUsers();
    }

    public  User findUserById(String id) {
        return userDAO.findUserById(id);
    }

    public  User findUserByLogin(String login) {
        return userDAO.findUserByLogin(login);
    }

    public  void deleteUserById(String id) {
//        users.removeIf(u -> u.getId().equals(id));
        userDAO.deleteUserById(id);
    }

    public  void updateUser(String id, String name, String surname, String password, String role, String email) {
        userDAO.updateUser(id,name,surname,password,role,email);
    }

    public void setUserRole(String userId, String role) {
        userDAO.setUserRole(userId,role);
    }

}
