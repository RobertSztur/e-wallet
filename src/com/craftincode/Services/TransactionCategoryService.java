package com.craftincode.Services;

import DAOs.TransactionCategoryDAO;
import com.craftincode.Model.Transaction;
import com.craftincode.Model.TransactionCategory;

import java.util.ArrayList;
import java.util.List;

public class TransactionCategoryService {
    public List<TransactionCategory> getAll(){
        return TransactionCategoryDAO.getAll();
    }

    public static TransactionCategory getCategoryOfTransaction(String transactionId) {
       return TransactionCategoryDAO.getCategoryOfTransaction(transactionId);
    }


    public static void createNew (TransactionCategory transactionCategory){
        TransactionCategoryDAO.createNew(transactionCategory);

//        transactionCategories.add(transactionCategory);
    }

    public void delete(String id){
//        transactionCategories.removeIf(tc -> tc.getId().equals(id));
        TransactionCategoryDAO.delete(id);
    }

    private TransactionCategory getById(String categoryId) {
return TransactionCategoryDAO.getById(categoryId);
    }
}
