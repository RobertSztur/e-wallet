package com.craftincode;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter("/*")
public class SecurityFilter implements Filter {

        @Override
        public void init(FilterConfig filterConfig) throws ServletException {
        }

        @Override
        public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
            HttpServletRequest req = (HttpServletRequest) servletRequest;
            HttpServletResponse resp = (HttpServletResponse) servletResponse;
            String username= (String) req.getSession().getAttribute("username");
            String uri = req.getRequestURI();
            String url = req.getRequestURI();

            if (username != null || uri.endsWith("login.html") || uri.endsWith("loginServlet")
                    || url.endsWith("register.html") || url.endsWith("users") || url.endsWith("register")){
            filterChain.doFilter(req, resp);
            }else {
                resp.sendRedirect(resp.encodeRedirectURL(req.getContextPath() + "/login.html"));
}
        }
    @Override
    public void destroy() {
    }
    }
