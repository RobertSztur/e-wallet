<%@ page import="com.craftincode.Model.Account" %>
<%@ page import="com.craftincode.Services.AccountService" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: rober
  Date: 2019-10-07
  Time: 19:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>

<h1>All accounts</h1>

<%
    List<Account> accounts = (List<Account>) request.getAttribute("accounts");
%>

<table>
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Balance</th>
    </tr>

    <%
        for (Account account : accounts) {
    %>
    <tr>
        <td><%=account.getId()%></td>
        <td><%=account.getName()%></td>
        <td><%=account.getBalance()%></td>
    </tr>
    <%
        }
    %>
</table>

<hr>
<a href="homepage"><< Back to homepage</a>

</body>
</html>

