<%--
  Created by IntelliJ IDEA.
  User: rober
  Date: 2019-10-01
  Time: 10:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Create account</title>
</head>
<body>
<h1>Create account</h1>
<form method="post" action="CreateAccountServlet">
    <br>
    <label>
        Account name:
        <input type="text" name="name">
    </label>
    <br>
    <label>
        Balance:
        <input type="number" name="balance" value="0">
    </label>
    <br>
    <input type="submit" value="Create">
</form>
</body>
</html>
