
<%@ page import="com.craftincode.Services.AccountService" %>
<%@ page import="java.util.List" %>
<%@ page import="com.craftincode.Model.Account" %>
<%@ page import="java.math.BigDecimal" %>
<%@ page import="com.craftincode.Model.User" %>
<%@ page import="com.craftincode.Services.UserService" %><%--
  Created by IntelliJ IDEA.
  User: rober
  Date: 2019-09-30
  Time: 18:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Konta zalogowanego użytkownika</title>
    <style>
        h1 {
            text-align: center;
        }

        table {
            width: 100%;
            text-align: center;
            border-collapse: collapse;
        }

        th {
            color: red;
            font-weight: 900;
        }

        th, td {
            border: 2px solid black;
        }
    </style>
</head>
<body>
<h1>Zestawienie kont przypisanych do użytkownika</h1>
<%
    BigDecimal sum = (BigDecimal) request.getAttribute("sum");
    List<Account> accounts = (List<Account>) request.getAttribute("accounts");
%>
<table>
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Balance</th>
    </tr>

    <%
        for (Account account : accounts) {
    %>
    <tr>
        <td><%=account.getId()%></td>
        <td><%=account.getName()%></td>
        <td><%=account.getBalance()%></td>
        <td>
            <a href="account-details.jsp?id=<%=account.getId()%>">Open</a>
        </td>
    </tr>
<%}%>
</table>
<hr>
<h2>Suma środków na kontach<%=sum%></h2
<br/>
<a href="create-account.jsp">Add new account</a>
<br/>
<a href="homepage">Go to homepage</a>
</body>
</html>
