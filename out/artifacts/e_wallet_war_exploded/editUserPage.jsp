<%@ page import="com.craftincode.Model.User" %>
<%@ page import="com.craftincode.Services.UserService" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: rober
  Date: 2019-09-28
  Time: 19:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Edition page</title>
</head>
<body>
<h1>Welcome to user edition page</h1>
<%
    String id = request.getParameter("id");
    User userData = UserService.findUserById(id);
%>
<h1>Edit user with ID: <%=id%></h1>
<form method="post" action="editData">
    <input name="id" value="<%=userData.getId()%>" type="hidden">
    <input name="role" value="<%=userData.getRole()%>" type="hidden">

    <label>
        Name:
        <input type="text" name="name" value="<%=userData.getName()%>">
    </label>
    <br>
    <label>
        Surname:
        <input type="text" name="surname" value="<%=userData.getSurname()%>">
    </label>
    <br>
    <label>
        Password:
        <input type="password" name="password" value="<%=userData.getPassword()%>">
    </label>
    <br>
    <label>
        Email:
        <input type="email" name="email" value="<%=userData.getEmail()%>">
    </label>
    <br>

    <input type="submit" value="Save changes">

</form>

<a href="allusers.jsp">Go to main menu</a>
</body>
</html>
