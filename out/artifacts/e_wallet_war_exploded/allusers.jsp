<%@ page import="java.util.List" %>
<%@ page import="java.io.PrintWriter" %>
<%@ page import="com.craftincode.Model.User" %>
<%@ page import="com.craftincode.Services.UserService" %><%--
  Created by IntelliJ IDEA.
  com.craftincode.Model.User: rober
  Date: 2019-09-26
  Time: 17:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Tablica wszystkich użytkowników</title>
    <style>
        h1 {
            text-align: center;
        }

        table {
            width: 100%;
            text-align: center;
            border-collapse: collapse;
        }

        th {
            color: red;
            font-weight: 900;
        }

        th, td {
            border: 2px solid black;
        }
    </style>
</head>
<body>

<h1>Zestawienie użytkowników</h1>
<table>
    <tr>
        <th>Id</th>
        <th>Login</th>
        <th>Name</th>
        <th>Surname</th>
        <th>Role</th>
        <th>Email</th>
        <th>Accounts</th>
        <th>Transactions</th>
    </tr>
    <%
        List<User> allUsers = (List<User>) request.getAttribute("users");
        for (User user : allUsers) {
    %>
    <tr>
    <td><%=user.getId()%></td>
    <td><%=user.getLogin()%></td>
    <td><%=user.getName()%></td>
    <td><%=user.getSurname()%></td>
    <td><%=user.getRole()%></td>
    <td><%=user.getEmail()%></td>
        <td><%=user.getAccountIds()%></td>
        <td><%=user.getTransactionIds()%></td>
        <td>
            <a href="delete?id=<%=user.getId()%>">Delete</a>
        </td>
        <td>
            <a href="changeRole?id=<%=user.getId()%>&role=USER">Set USER role</a>
        </td>
        <td>
            <a href="changeRole?id=<%=user.getId()%>&role=ADMIN">Set ADMIN role</a>
        </td>
        <td>
            <a href="editUserPage.jsp?id=<%=user.getId()%>">Edit user</a>
        </td>
    </tr>
<% }%>
</table>
<br>
<a href="homepage">Go back to homepage</a>
</body>
</html>
