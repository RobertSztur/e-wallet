<%@ page import="com.craftincode.Model.Account" %>
<%@ page import="java.util.List" %>
<%@ page import="com.craftincode.Services.AccountService" %>
<%@ page import="com.craftincode.Model.Transaction" %>
<%@ page import="com.craftincode.Services.TransactionService" %><%--
  Created by IntelliJ IDEA.
  User: rober
  Date: 2019-10-01
  Time: 12:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--wyświetlającej szczegóły konta oraz listę wszystkich transakcji.--%>
<html>
<head>
    <title>Account details</title>
</head>
<body>

<h1>Account details</h1>

<%
    String accountId = request.getParameter("id");
    Account account = AccountService.getById(accountId);
%>
<b>Id:</b> <%=accountId%>
<br>
<b>Name:</b> <%=account.getName()%>
<br>
<b>Balance:</b> <%=account.getBalance()%>

<hr>

<h3>Transactions:</h3>

<table>
    <tr>
        <th>ID</th>
        <th>Amount</th>
        <th>Description</th>
    </tr>
    <%
        List<Transaction> transactions = TransactionService.getTransactionsForAccount(accountId);
        for (Transaction transaction : transactions) {
    %>
    <tr>
        <td><%=transaction.getId()%></td>
        <td><%=transaction.getAmount()%></td>
        <td><%=transaction.getDescription()%></td>
    </tr>
    <%
        }
    %>
</table>

<hr>
<a href="create-transaction.jsp?accountId=<%=accountId%>">Add new transaction</a>

<hr>
<a href="user-accounts.jsp"><< Back to accounts</a>

</body>
</html>

