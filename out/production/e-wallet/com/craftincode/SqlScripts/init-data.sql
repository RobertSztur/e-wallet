use e_wallet;

insert into account
values
('acc1','slużbowe', 1000),
('acc2','prywatne', 1000),
('acc3','slużbowe', 2000);

insert into user_account
value
(1,'acc1'),
(1,'acc2'),
(2,'acc3');

insert into transaction(id,description,amount,date, transaction_category_id,user_id)
values
(1,'paliwo', 200,'2019-12-01',1,1),
(2,'jedzenie', 200, '2019-12-02',2,2);

insert into transaction_category
values
(1,'transport'),
(2,'życie');

insert into user(id,name,surname,login,password,role,email)
values
(1,'Robert', 'Kowalski', 'admin', 'admin', 'Admin','admin@ewallet.pl'),
(2,'Michał', 'Kowalski', 'user', 'user', 'User','user1@ewallet.pl'),
(3,'Piotr', 'Nowak', 'user2', 'user2', 'User','user2@ewallet.pl');