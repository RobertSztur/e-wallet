create table transaction_category (
                                      id varchar(100) primary key,
                                      name varchar(100)
);

create table account(
                        id varchar(100) primary key,
                        name varchar(100),
                        balance numeric
);

create table user(
                     id varchar(100) primary key,
                     name varchar(100),
                     surname varchar(100),
                     login varchar(100),
                     password varchar(100),
                     role varchar(100),
                     email varchar(100)
);

create table transaction(
                            id varchar(100) primary key,
                            description varchar(100),
                            amount numeric,
                            date timestamp,
                            transaction_category_id varchar(100),
                            account_id varchar(100),
                            user_id varchar(100),
                            foreign key (transaction_category_id) references transaction_category(id),
                            foreign key (account_id) references account(id),
                            foreign key (user_id) references user(id)
);
create table user_account (
    user_id varchar(100),
    account_id varchar(100),
    foreign key (user_id) references user(id),
    foreign key (account_id)references account(id),
    primary key (user_id, account_id)
);